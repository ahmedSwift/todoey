//
//  ViewController.swift
//  todoey
//
//  Created by afaq on 21/02/1440 AH.
//  Copyright © 1440 afaqafaqafaqnone. All rights reserved.
//

import UIKit
import RealmSwift
class ToDolistVC: UITableViewController {

    let realm = try! Realm()
    var itemAray: Results<Item>?
    var selectdCat : Category?{
        didSet{
            loadItems()
        }
    }
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    //Mark: - tableview data sources
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DoToItemCell", for: indexPath)
        
        cell.textLabel?.text = itemAray?[indexPath.row ].title ?? ""
        
        if itemAray?[indexPath.row].done == true{
            cell.accessoryType = .checkmark
        }else{
            cell.accessoryType = .none
        }
        return cell
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itemAray?.count ?? 1
    }
    
    //MARK - tableView delegate methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let item = itemAray?[indexPath.row]{
           
            do{
                try realm.write {
                item.done = !item.done
            }
            }catch{
                print("error\(error)")
            }
        }
        
        tableView.reloadData()
        tableView.deselectRow(at: indexPath, animated: true)
        
    }


    //MARK- add todo
    @IBAction func add(_ sender: Any) {
    
        var TF = UITextField()
        
        let alert = UIAlertController(title: "أضف مهمة",message:"", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "أضف مهمة", style: .default) { (action) in
            
            
            if let category = self.selectdCat {
            do {
                try self.realm.write {
                    let newItem = Item()
                    newItem.title = TF.text!
                    category.items.append(newItem)
                }
            }   catch{
                print ("error saving context \(error)")
            }
            }
            
            
            self.tableView.reloadData()
            
        }
        
        alert.addAction(action)
        alert.addTextField { (alertTF) in
            TF = alertTF
            TF.placeholder = "أكتب مهمتك"
        }
        present(alert,animated: true,completion: nil)
        
    
    }
    
    
    func loadItems() {
        itemAray = selectdCat?.items.sorted(byKeyPath: "title", ascending: true)
    }
    


}

extension ToDolistVC : UISearchBarDelegate{
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        itemAray = itemAray?.filter("title CONTAINS[cd] %@", searchBar.text!).sorted(byKeyPath: "title", ascending: true)
    }

    // rsigning to View

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text?.count == 0{
            loadItems()
            tableView.reloadData()
                DispatchQueue.main.async {
                    searchBar.resignFirstResponder()
            }
            }

        }

    }


