//
//  categoeryVC.swift
//  todoey
//
//  Created by afaq on 03/03/1440 AH.
//  Copyright © 1440 afaqafaqafaqnone. All rights reserved.
//

import UIKit
import RealmSwift

class categoeryVC: UITableViewController {

    let realm = try! Realm()
    
    var catArray : Results<Category>?
    

    override func viewDidLoad() {
        super.viewDidLoad()

        loadItems()
       
    }

    //MARK: - TableView DataSource Methods
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CatCell", for: indexPath)
        
        cell.textLabel?.text = catArray?[indexPath.row ].name ?? "no Categorys"
        return cell
    }
    
        override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return catArray?.count ?? 1
    }
    
    
    //MARK: - Add new categoers
    
    @IBAction func addButtonPressed(_ sender: UIBarButtonItem) {
        
        var TF = UITextField()
        
        let alert = UIAlertController(title: "أضف قسم جديد",message:"", preferredStyle: .alert)
        
        let action = UIAlertAction(title: "أضف مهمة", style: .default) { (action) in
            let newCat = Category()
            newCat.name = TF.text!
            self.save(category: newCat)
            }
        
    alert.addAction(action)
    alert.addTextField { (alertTF) in
    TF = alertTF
    TF.placeholder = "أكتب مهمتك"

    }
      present(alert,animated: true,completion: nil)
   
    }

    
    
    //MARK: - Delegating methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "goToItems", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let desnationVC = segue.destination as! ToDolistVC
        if let indexPath = tableView.indexPathForSelectedRow{
        desnationVC.selectdCat = catArray?[indexPath.row]
        }
        }
    
    
    
    //MARK: - Functions
    
    func save(category: Category){
        
        do {
            try realm.write {
                realm.add(category)
            }
        }   catch{
            print ("error saving context \(error)")
        }
        
        
        self.tableView.reloadData()
    }
    
    func loadItems(){
        catArray = realm.objects(Category.self)
    }
    
//End of class
}
