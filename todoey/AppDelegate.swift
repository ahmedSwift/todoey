//
//  AppDelegate.swift
//  todoey
//
//  Created by afaq on 21/02/1440 AH.
//  Copyright © 1440 afaqafaqafaqnone. All rights reserved.
//

import UIKit
import CoreData
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        
        
        
        do{
            let realm = try Realm()
            }
        catch{
            print("error creating realm object \(error)")
        }
        
        return true
    }

    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        
    }
    
 }

    




