//
//  Item.swift
//  todoey
//
//  Created by afaq on 05/03/1440 AH.
//  Copyright © 1440 afaqafaqafaqnone. All rights reserved.
//

import Foundation
import RealmSwift

class Category: Object {
    
    @objc dynamic var name : String = ""
    let items = List<Item>()
}
