//
//  Item.swift
//  todoey
//
//  Created by afaq on 05/03/1440 AH.
//  Copyright © 1440 afaqafaqafaqnone. All rights reserved.
//

import Foundation
import RealmSwift

class Item: Object {
 
    @objc dynamic var title : String = ""
    @objc dynamic var done : Bool = false
    
    var parentCategory = LinkingObjects(fromType: Category.self, property: "items")
}
